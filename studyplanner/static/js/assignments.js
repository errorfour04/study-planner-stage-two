// import "./jquery-3.2.1";

// Stops the task headers from closing while you're typing in them
function bindTaskHeaderFocus() {
  // $(".task-header").on("click", function() {
  //   $(this).on("focus", function() {
  //      $(this).attr("disabled", "true");
  //   })
    // alert("Focused");
  // });
}

// Capture typing in the note input
function bindNoteInputChange() {
  $(".note-input").on("keydown", function(e) {
    $(this).closest(".task-header").attr("disabled", "true");

    // block enter presses
    if (e.keyCode == 13) e.preventDefault();
  })

  $(".note-input").on("focusout", function() {
    $(this).closest(".task-header").removeAttr("disabled");
  })
}

// Make sure the correct assignment is added to
function bindCompleteActivityClick() {
  $("button.complete-activity-button").on("click", function() {
    const id = $(this).attr("activity-id");
    const name = $(this).attr("activity-name");
    const contribution = $(this).attr("activity-contribution");

    // Set the title of the doActivityModal
    $("#modal-activity-name").text(name);

    // Set the contribution of the doActivityModal
    $("#modal-activity-contribution").attr("value", contribution);

    // Set the id of the doActivityModal
    $("#hidden-activity-id").attr("value", id);
  })
}

window.addEventListener("load", function() {
  bindCompleteActivityClick();
  bindTaskHeaderFocus();
  bindNoteInputChange();
})

// Date Constants
const months = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
];
const oneWeek = 7 * 24 * 60 * 60 * 1000; // One week in milliseconds

// Format date for display on the gantt chart
const formatDate = date => "" + date.getDate() + "-" + months[date.getMonth()];

// Constants
const incompleteColour = "rgb(244, 66, 95)";
const completeColour = "rgb(49, 234, 126)";
const noneColour = "rgb(30, 30, 30)";

const ganttIncomplete = /*"rgb(244, 86, 65)"*/ "rgb(0, 0, 0)";
const ganttComplete = /*"rgb(66, 134, 244)"*/ "rgb(255, 194, 0)";
const ganttLineColour = "rgb(255, 255, 255)";
const ganttXStart = 0.25;
const ganttXEnd = 0.95;
const ganttYStart = 0.05;
const ganttYEnd = 0.8;

const milestoneIncomplete = "rgba(240, 240, 240, 0.7)";
const milestoneComplete = "rgb(239, 218, 110)";

// Display Milestone Star
function displayCompletionStar(canvasID, progress = 0.62) {
  if (progress > 1) progress = 1;
  if (progress < 0) progress = 0;

  const canvas = document.getElementById(canvasID);

  const ctx = canvas.getContext("2d");
  const width = canvas.width;
  const height = canvas.height;

  // Get star coordinates
  const x1 = (width * 1) / 6;
  const x2 = (width * 2) / 6;
  const x3 = (width * 3) / 6;
  const x4 = (width * 4) / 6;
  const x5 = (width * 5) / 6;
  const x6 = (width * 6) / 6;

  const y1 = (height * 1) / 3;
  const y2 = (height * 2) / 3;
  const y3 = (height * 3) / 3;
  // const y4 = height * 1.0;

  // Create complete/incomplete portions
  const twoTone = ctx.createLinearGradient(0, 0, width, 0);
  twoTone.addColorStop(0, milestoneComplete);
  twoTone.addColorStop(progress, milestoneComplete);
  twoTone.addColorStop(progress, milestoneIncomplete);
  twoTone.addColorStop(1, milestoneIncomplete);

  ctx.fillStyle = twoTone;
  ctx.strokeStyle = "rgba(255, 255, 255, 0.5)";
  ctx.lineWidth = 1.5;

  // Drawing Star
  ctx.beginPath();

  ctx.moveTo(0, y1);
  ctx.lineTo(x2, y1);
  ctx.lineTo(x3, 0);
  ctx.lineTo(x4, y1);
  ctx.lineTo(x6, y1);
  ctx.lineTo(x5 - 0.5 * x1, y2 - 0.25 * y1);
  ctx.lineTo(x5, y3);
  ctx.lineTo(x3, y2 + 0.35 * y1);
  ctx.lineTo(x1, y3);
  ctx.lineTo(x1 + 0.5 * x1, y2 - 0.25 * y1);
  ctx.lineTo(0, y1);

  ctx.fill();
  // ctx.stroke();

  // Write Text
  ctx.fillStyle = "black";
  ctx.font = "bold 12px Arial";
  ctx.textAlign = "center";
  ctx.fillText(`${progress * 100}%`, width * 0.5, 6 + height * 0.5);
}

// Display a ring that shows the completion of a task
function displayCompletionRing(canvasID, cmp, tot, proportion = 0.9) {
  const canvas = document.getElementById(canvasID);

  const ctx = canvas.getContext("2d");
  ctx.lineWidth = 5;

  // Get dimensions of canvas
  const width = canvas.width;
  const height = canvas.height;
  const proportionOfWidth =
    proportion > 0 && proportion <= 1 ? proportion : 0.9; // validation

  // Validate task completion
  const completion = cmp / tot || 0;
  if (cmp < 0) cmp = 0;
  if (cmp > 1) cmp = 1;

  // radiusProportion determines how much of the canvas is filled
  const radius = (width * proportionOfWidth) / 2;
  const startAngle = -Math.PI / 2;
  const endAngle = startAngle + 2 * Math.PI * completion;

  if (tot == 0) {
    // User hasn't specified a completion amount for their task
    ctx.beginPath();
    ctx.strokeStyle = noneColour;
    ctx.arc(width / 2, height / 2, radius, 0, Math.PI * 2);
    ctx.stroke();
  } else {
    // Draw red circle for incomplete portion of task
    ctx.beginPath();
    ctx.strokeStyle = incompleteColour;
    ctx.arc(width / 2, height / 2, radius, 0, Math.PI * 2);
    ctx.stroke();

    // Draw green circle for complete portion of task
    ctx.beginPath();
    ctx.strokeStyle = completeColour;
    ctx.arc(width / 2, height / 2, radius, startAngle, endAngle);
    ctx.stroke();
  }
}

// Draw the Gantt Chart

// Creates the axes
function drawAxes(ctx, width, height, tasks, weeks) {
  ctx.strokeStyle = ganttLineColour;

  const xStart = width * ganttXStart;
  const xEnd = width * ganttXEnd;
  const yStart = height * ganttYStart;
  const yEnd = height * ganttYEnd;
  const xSize = xEnd - xStart;
  const ySize = yEnd - yStart;
  const ps = 6; // ps = prongSize

  ctx.beginPath();
  // y axis
  ctx.moveTo(xStart, yStart);
  ctx.lineTo(xStart, yEnd);

  // x axis
  ctx.moveTo(xStart - ps, yEnd - ps);
  ctx.lineTo(xEnd, yEnd - ps);

  // draw spokes on y axis
  for (i = 0; i < tasks; i++) {
    ctx.moveTo(xStart, yStart + (ySize * i) / tasks);
    ctx.lineTo(xStart - ps, yStart + (ySize * i) / tasks);
  }

  // draw spokes on x axis
  for (i = 0; i < weeks; i++) {
    ctx.moveTo(xStart + (xSize * (i + 1)) / weeks, yEnd);
    ctx.lineTo(xStart + (xSize * (i + 1)) / weeks, yEnd - ps);
  }

  ctx.stroke();

  ctx.beginPath();
  ctx.lineWidth = 0.5;
  for (i = 0; i < weeks; i++) {
    ctx.moveTo(xStart + (xSize * (i + 1)) / weeks, yEnd);
    ctx.lineTo(xStart + (xSize * (i + 1)) / weeks, yStart);
  }
  ctx.stroke();
}

// Put labels on the axes
function drawLabels(ctx, width, height, tasks, startDate, weeks) {
  let date = new Date(startDate);

  const xStart = width * ganttXStart;
  const xEnd = width * ganttXEnd;
  const yStart = height * ganttYStart;
  const yEnd = height * ganttYEnd;
  const xSize = xEnd - xStart;
  const ySize = yEnd - yStart;
  const ps = 6; // ps = prongSize

  // x axis
  ctx.textAlign = "center";
  for (i = 0; i <= weeks; i++) {
    ctx.fillStyle = ganttLineColour;
    ctx.fillText(
      formatDate(date),
      xStart + (xSize * i) / weeks,
      yEnd + 20,
      (xSize * 0.9) / weeks
    );
    date.setTime(date.getTime() + oneWeek); // increment the date
  }

  date.setTime(date.getTime() - oneWeek); // Correct the end date

  // y axis
  ctx.textAlign = "right";

  for (i = 0; i < tasks.length; i++) {
    ctx.fillStyle = ganttLineColour;

    // Write the labels
    ctx.fillText(
      tasks[i].title,
      xStart - ps,
      yStart + 6 + (ySize * 0.5) / tasks.length + (ySize * i) / tasks.length,
      width * ganttXStart * 0.9 // Max length of the labels
    );

    // Draw the bars
    drawBar(
      ctx,
      tasks[i],
      xSize,
      xStart,
      yStart + (ySize * 0.5) / tasks.length + (ySize * i) / tasks.length,
      new Date(startDate), // Create date object
      date // by now, 'date' refers to the end date
    );
  }

  // Display the key
  ctx.fillStyle = ganttLineColour;
  ctx.textAlign = "left";

  ctx.fillText("Complete", width * 0.35, height * 0.97);
  ctx.fillText("Incomplete", width * 0.55, height * 0.97);

  ctx.fillStyle = ganttComplete;
  ctx.fillRect(width * 0.35 - 12, height * 0.97 - 10, 10, 10);

  ctx.fillStyle = ganttIncomplete;
  ctx.fillRect(width * 0.55 - 12, height * 0.97 - 10, 10, 10);
}

// Draw bars
function drawBar(ctx, task, xSize, x, y, start, end) {
  const barHeight = 10;

  const timePeriod = end - start;
  let taskStart = (task.startDate - start) / timePeriod;
  let taskEnd = (task.endDate - start) / timePeriod;

  if (taskStart < 0) taskStart = 0;
  if (taskEnd > 1) taskEnd = 1;

  const barWidth = Math.max(5, xSize * (taskEnd - taskStart));

  // Incomplete portion of task
  ctx.fillStyle = ganttIncomplete;
  ctx.fillRect(x + xSize * taskStart, y - barHeight / 2, barWidth, barHeight);

  // Complete portion of task
  ctx.fillStyle = ganttComplete;
  ctx.fillRect(
    x + xSize * taskStart,
    y - barHeight / 2,
    barWidth * task.completion,
    barHeight
  );
}

// Combinbes everything to create full gantt chart
function displayGanttChart(
  canvasID,
  tasks,
  startDate = "2019-03-20",
  weeks = 8
) {
  const canvas = document.getElementById(canvasID);

  const ctx = canvas.getContext("2d");
  ctx.lineWidth = 1.5;
  ctx.font = "12px Arial";

  // Get dimensions of canvas
  const width = canvas.width;
  const height = canvas.height;

  drawAxes(ctx, width, height, tasks.length, weeks);
  drawLabels(ctx, width, height, tasks, startDate, weeks);
}

// Variables that store the data
// let ganttChartTasks = [
//   { title: "Task One" },
//   { title: "Task One" },
//   { title: "Task One" },
//   { title: "Task One" },
//   { title: "Task One has a very long name" }
// ];
