// import "./jquery-3.2.1";

// Semester file
function loadFileAsText(id) {
  const fileToLoad = document.getElementById(id).files[0];

  const fileReader = new FileReader();

  fileReader.onload = function(fileLoadedEvent) {
    const textFromFileLoaded = fileLoadedEvent.target.result;
    // alert(textFromFileLoaded);
    // document.getElementById(id).value = textFromFileLoaded;
    document.getElementById(
      "semester-file-contents"
    ).value = textFromFileLoaded;
  };

  try {
    fileReader.readAsText(fileToLoad, "UTF-8");
  } catch (err) {
    alert("Something is wrong with your semester file");
  }
}

$("#semester-file").on("change", function(e) {
  // alert(loadFileAsText("semester-file"));
  loadFileAsText("semester-file");
});

// Ensure assignments are added to the correct module
$(".add-assignment-button").on("click", function(e) {
  // Get the associated module of the button that was clicked
  const $button = $(this);
  const moduleID = $button.attr("module-id");

  // Set the hidden input value
  const hidden = $("#hidden-module-id");
  hidden.attr("value", moduleID);
});

// Toggle profile tabs with a select
$("#profile-select").on("change", e => {
  const selected = $("#profile-select option:selected").attr("data-target");
  $('.nav-tabs a[href="' + selected + '"]').tab("show");
});

// // Toggle extra info tabs
$("ul.nav-pills li a").click(function(e) {
  $("ul.nav-pills li.active").removeClass("active");
  $(this)
    .parent("li")
    .addClass("active");
});

// la = "left arrow" ra = "right arrow"
// Calendar back button
$("a.la").on("click", e => {
  e.preventDefault();
  const tabs = $(".tab-content .active .calendar-area .nav-tabs a");
  const $selected = tabs.filter(".active");
  const index = tabs.index($selected);
  const next = (tabs.length + index - 1) % tabs.length;

  // Show previous month
  $(".tab-content .active .calendar-area .nav-tabs a:eq(" + next + ")").tab(
    "show"
  );
});

// Calendar forward button
$("a.ra").on("click", e => {
  e.preventDefault();
  const tabs = $(".tab-content .active .calendar-area .nav-tabs a");
  const $selected = tabs.filter(".active");
  const index = tabs.index($selected);
  const next = (index + 1) % tabs.length;

  // Show previous month
  $(".tab-content .active .calendar-area .nav-tabs a:eq(" + next + ")").tab(
    "show"
  );
});
