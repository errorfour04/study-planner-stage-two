from django.shortcuts import render, redirect, get_object_or_404
from datetime import datetime

from constants.types import STUDY_TYPES

from .models import Assignment
from tasks.models import Task


def assignment(request, assignment_id):
    # assignment = Assignment.objects.get(id=assignment_id)
    _assignment = get_object_or_404(Assignment, pk=assignment_id)

    context = {
        'assignment': _assignment,
        'types': STUDY_TYPES
    }

    return render(request, 'assignments/assignment.html', context)


def create_task(request, assignment_id):
    if request.method == 'POST':
        assignment = Assignment.objects.get(id=assignment_id)
        title = request.POST['title']
        description = request.POST['description']
        study_type = str(request.POST['studytype']).upper()
        target = request.POST['target']
        dependencies = request.POST.getlist('dependencies')
        start_date = request.POST['start']
        end_date = request.POST['end']

        # Create the new object
        task = Task(
            assignment=assignment,
            title=title,
            description=description,
            study_type=study_type,
            target=target,
            start_date=start_date,
            end_date=end_date
        )

        task.save()

        for dep in dependencies:
            task.add_dependency(dep)

        return redirect('/assignments/{}'.format(assignment_id))

def do_activity(request, assignment_id):
    from activities.models import Activity, ActivityLogs

    contribution = None
    time_taken = None

    # POST REQUEST
    if request.method == 'POST':

        # save to logs
        contribution = int(request.POST['contribution'])
        time_taken = int(request.POST['time_taken'])
        activity_id = int(request.POST['activityid'])

        activity = get_object_or_404(Activity, pk=activity_id)

        tasks = activity.associated_tasks.all()

        a = ActivityLogs(
            activity_id=Activity.objects.get(pk=activity_id),
            name=Activity.objects.get(pk=activity_id).name,
            time_taken=time_taken,
            contribution=contribution
        )
        a.save()


        # increment time dedicated to activity as well as contribution to tasks
        actToSave = Activity.objects.get(pk=activity_id)
        actToSave.do_activity(time_taken, contribution)
        actToSave.contribution = contribution
        actToSave.save()

    return redirect('/assignments/{}'.format(assignment_id))



def view_activity(request):
    name = "a5"






    # data to pass onto the web page
    context = {
        'activity': name
    }



    return render(request, 'profiles/addActivityModal.html', context)




def add_activity(request, assignment_id):
    from constants.types import STUDY_TYPES
    from activities.models import Activity


    # NEED TO ADD BASED UPON ASSIGNMENT



    # boolean value to determine that the study types match
    typesMatch = True


    #get activities
    activities = [activity for activity in Activity.objects.all()]

    # Get tasks
    tasks = [task for task in Task.objects.all()]






    #POST REQUEST
    if request.method == 'POST':

        name = request.POST['name']
        contribution = int(request.POST['contribution'])
        time_taken = request.POST['time_taken']

        if(time_taken == ''):
            print("hi")
            time_taken = 0

        # only returns selected tasks
        got_tasks = request.POST.getlist('task')

        # only returns the selected study type
        got_type = request.POST.getlist('atype')


        # ensure that the study type matches the types of the tasks

        for task_id in got_tasks:

            task_from_id = Task.objects.get(id=task_id)

            if(got_type[0].upper() != task_from_id.study_type):
                typesMatch = False


        # add activity information to database
        if(typesMatch is True):
            a = Activity(assignment = Assignment.objects.get(pk=assignment_id), name = name, contribution = contribution, time_taken = time_taken,\
                         study_type = got_type[0].upper())
            a.save()


            for task_id in got_tasks:
                task_from_id = Task.objects.get(id=task_id)
                a.associated_tasks.add(task_from_id)

            print(a)








    # data to pass onto the web page
    context = {
        'activities': activities,
        'tasks': tasks,
        'types': STUDY_TYPES,
        'validateType': typesMatch
    }

    return redirect('/assignments/{}'.format(assignment_id))


# adding a milestone
def add_milestone(request, assignment_id):
    from milestones.models import Milestone

    tasks = [task for task in Task.objects.all()]

    # boolean value to determine if deadline is ahead of tasks to complete deadline
    deadlineValid = True


    if request.method == "POST":
        name = request.POST['name']
        got_tasks = request.POST.getlist('task')

        date = request.POST['date']
        time = request.POST['time']

        concat = date + ' ' + time
        formatDateTime = datetime.strptime(concat, '%Y-%m-%d %H:%M')



        task_count = 0

        # iterate through all tasks
        for task_id in got_tasks:
            task_from_id = Task.objects.get(id=task_id)
            task_from_id.end_date = task_from_id.end_date.replace(tzinfo = None)


            if task_from_id.end_date.replace(tzinfo = None)> formatDateTime:
                deadlineValid = False

            task_count += 1


        if deadlineValid is True:
            m = Milestone(assignment=Assignment.objects.get(pk=assignment_id), name=name,
                          deadline=formatDateTime)
            m.save()

            count = 0

            for task_id in got_tasks:

                task_from_id = Task.objects.get(id=task_id)
                m.tasks_to_complete.add(task_from_id)


                if task_from_id.completed is True:
                      count += 1


            # set progress
            progress = count / task_count
            if progress == 1.0:
                m.complete = True
                m.save()

            m.progress = progress

            m.save()

    context = {'tasks': tasks,
               'validDeadline': deadlineValid}

    return redirect('/assignments/{}'.format(assignment_id))

# viewing a milestone, allows the completion of tasks
def view_milestone(request, mil_name):
    from activities.models import Activity

    # get all activities
    activities = Activity.objects.all()

    # to filter out relevant activities
    activitiesToShow = []


    m = Milestone.objects.get(name = mil_name)

    tasks = m.tasks_to_complete.all()

    for activity in activities:
        for task in tasks:
            if task in activity.associated_tasks.all():
                activitiesToShow.append(activity)




    context = {'activities': activitiesToShow,
               'tasks': tasks,
               'milestone': m}
    return render(request, 'profiles/viewMilestoneModal.html', context)
