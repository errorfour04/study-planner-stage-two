from django.db import models
from tasks.models import Task
from assignments.models import Assignment
from constants.types import STUDY_TYPES
from datetime import datetime



class Activity(models.Model):
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    study_type = models.CharField(max_length=50, choices=STUDY_TYPES)
    contribution = models.IntegerField()
    time_taken = models.IntegerField(blank=True)
    associated_tasks = models.ManyToManyField(Task)

    @property
    def study_type_fmt(self):
        return str(self.study_type).lower().capitalize()

    # complete an activity with a set contribution
    def do_activity(self, time_taken, contribution):
        from milestones.models import Milestone

        # increment time taken
        self.time_taken += time_taken


        # deduct from relevant tasks
        for task in self.associated_tasks.all():

            print(task.amount_complete)
            print(task.target)
            print(contribution)

            if task.amount_complete + contribution >= task.target:
                task.amount_complete = task.target
                task.completed = True

                for milestone in Milestone.objects.all():
                    if task in milestone.tasks_to_complete.all():
                        milestone.set_progress()
            else:
                task.amount_complete += contribution


            task.time_spent += time_taken
            task.save()


        self.save()




    # state whether an activity is complete
    def is_complete(self):
        for task in self.associated_tasks:
            if not(task.completed):
                return False;

        return True;



    def __str__(self):
        return self.name


class ActivityLogs(models.Model):
    activity_id = models.ForeignKey(Activity, null=True, on_delete=models.SET_NULL)
    name = models.CharField(max_length=100)
    contribution = models.IntegerField()
    time_taken = models.IntegerField()
    time_complete = models.DateTimeField(default=datetime.now, blank=True)
