from django.contrib import admin
from .models import Activity


class ActivityAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'contribution', 'assignment')
    list_display_links = ('id', 'name')
    list_filter = ('assignment',)

    list_per_page = 25


admin.site.register(Activity, ActivityAdmin)
