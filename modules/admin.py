from django.contrib import admin
from .models import Module


class ModuleAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'profile')
    list_display_links = ('id', 'name')
    list_filter = ('profile',)
    list_per_page = 25


admin.site.register(Module, ModuleAdmin)
