from django.contrib import admin
from .models import Milestone


class MilestoneAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'progress', 'complete', 'assignment')
    list_display_links = ('id', 'name')
    list_filter = ('complete', 'assignment')

    list_per_page = 25


admin.site.register(Milestone, MilestoneAdmin)
