from django.db import models
from datetime import datetime
from tasks.models import Task
from assignments.models import Assignment


class Milestone(models.Model):
    assignment = models.ForeignKey(Assignment, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    deadline = models.DateTimeField()
    progress = models.DecimalField(max_digits=3, decimal_places=2, default=0.0)
    complete = models.BooleanField(default=False)
    tasks_to_complete = models.ManyToManyField(Task)

    @property
    def is_overdue(self):
        return (not self.complete) and (
                self.deadline.replace(tzinfo=None) < datetime.now()
        )

    def set_progress(self):
        complete_task_count = 0

        for task in self.tasks_to_complete.all():
            if task.completed:
                complete_task_count += 1

            self.progress = complete_task_count / len(self.tasks_to_complete.all())
            if self.progress >= 1: self.complete = True
            self.save()

    def __str__(self):
        return self.name
