import calendar
from datetime import datetime, timedelta
from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import auth
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required

from .models import Profile
from modules.models import Module
from assignments.models import Assignment
from tasks.models import Task

from constants.colours import MODULE_COLOURS, MODULE_STYLES, MODULE_ICONS
from constants.types import ASSIGNMENT_TYPES


@login_required(login_url='login')
def dashboard(request, user_id):
    user = get_object_or_404(User, pk=user_id)

    # Make sure user has authority to access the page
    if auth.get_user(request).id == user.id:
        # POST REQUEST
        if request.method == 'POST':
            title = request.POST['title']
            semester_file = request.POST['semesterfilecontents']
            modules = request.POST['modules']

            # semester_file = str(semester_file).split("\n")
            # for line in semester_file:
            #     print(line)

            # Get individual module titles
            unique_modules = []
            for mod in str(modules).split(', '):
                if not unique_modules.__contains__(mod.lower()):
                    unique_modules.append(mod)

            # Create profile
            new_profile = Profile.objects.create(account=user, title=title)
            if len(str(semester_file).strip()) > 0:
                new_profile.load_semester_file(semester_file)

            # Add modules
            for mod in unique_modules:
                new_profile.add_module(mod)

            # return redirect('/profiles/{}'.format(user.id))

        # GET REQUEST

        # Get study profiles
        profiles = Profile.objects.filter(account=user)

        # Get modules
        modules = None

        if profiles:
            modules = [mod for mod in Module.objects.all()
                       if mod.profile in profiles]

            # Assign module colours
            for mod in modules:
                mod.colour = MODULE_COLOURS[modules.index(
                    mod) % len(MODULE_COLOURS)]

                mod.style = MODULE_STYLES[modules.index(
                    mod) % len(MODULE_STYLES)]

            # Assign module icons
            for mod in modules:
                mod.icon = MODULE_ICONS[modules.index(
                    mod) % len(MODULE_ICONS)]

        # Get assignments
        assignments = None
        if modules:
            assignments = [assignment for assignment in Assignment.objects.all(
            ) if assignment.module in modules]

        # Get tasks
        tasks = None
        if assignments:
            tasks = [task for task in Task.objects.all(
            ) if task.assignment in assignments]

            # Add extra field to task for easier templating
            for task in tasks:
                task.profile = task.assignment.module.profile
                task.colour = next(mod.colour for mod in modules if (
                    task.assignment.module == mod))
                task.style = next(mod.style for mod in modules if (
                    task.assignment.module == mod))

        # Dates for the calendar
        dates = []
        current_month = datetime.now().month - 2
        current_date = datetime(datetime.now().year, current_month, 1)

        for i in range(5):
            month = {}
            current_date = datetime(current_date.year, current_month, 1)
            current_date -= timedelta(datetime.weekday(current_date))

            month.update({'row0': ['x', 'x', 'x', 'x', 'x', 'x', 'x']})
            month.update({'row1': ['x', 'x', 'x', 'x', 'x', 'x', 'x']})
            month.update({'row2': ['x', 'x', 'x', 'x', 'x', 'x', 'x']})
            month.update({'row3': ['x', 'x', 'x', 'x', 'x', 'x', 'x']})
            month.update({'row4': ['x', 'x', 'x', 'x', 'x', 'x', 'x']})

            for key in month.keys():
                for i in range(7):
                    month[key][i] = current_date
                    # Increment the date
                    current_date += timedelta(days=1)

            month.update({'year': current_date.year})
            month.update({'month': calendar.month_name[current_month]})
            dates.append(month)
            # set the new month
            current_month = (current_date + timedelta(days=1)).month

        context = {
            'user': user,
            'profiles': profiles,
            'modules': modules,
            'assignments': assignments,
            'tasks': tasks,
            'dates': dates,
            'types': ASSIGNMENT_TYPES
        }

        return render(request, 'profiles/dashboard.html', context)
    else:
        auth.logout(request)
        return redirect('login')


@login_required(login_url='login')
def create_assignment(request):
    if request.method == 'POST':
        module_id = int(request.POST['moduleid'])
        title = request.POST['title']
        assignment_type = str(request.POST['atype']).upper()
        deadline = request.POST['deadline']
        weighting = request.POST['weighting']

        # Create the new assignment
        if Module.objects.get(pk=module_id).profile.account != auth.get_user(request):
            print("THE IDs DO NOT MATCH")
        else:
            assignment = Assignment(
                module=Module.objects.get(pk=module_id),
                title=title,
                assignment_type=assignment_type,
                deadline=deadline,
                weighting=weighting
            )

            assignment.save()

            return redirect('/assignments/{}'.format(assignment.id))
