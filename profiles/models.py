from django.db import models
from datetime import datetime
from django.contrib.auth.models import User


class Profile(models.Model):
    account = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    created_at = models.DateTimeField(default=datetime.now)

    # Create a module for given profile
    def add_module(self, name):
        from modules.models import Module

        # Check that duplicate modules aren't made within profile
        if not Module.objects.filter(profile=self, name__iexact=name).exists():
            Module.objects.create(profile=self, name=name)

    # Initialise from semester file
    def load_semester_file(self, file):
        from modules.models import Module
        from assignments.models import Assignment

        semester_file = str(file).split("\n")

        for line in semester_file:
            items = line.split(",")

            mod = None

            # Get/Create a Module
            if not Module.objects.filter(profile=self, name__iexact=items[0]).exists():
                mod = Module.objects.create(profile=self, name=items[0])
            else:
                mod = Module.objects.filter(
                    profile=self, name__iexact=items[0])[0]

            # Create Assignments
            try:
                assignment = Assignment(
                    module=mod,
                    title=items[1],
                    assignment_type=str(items[2]).upper(),
                    deadline=datetime.strptime(items[3], "%d/%m/%y"),
                    weighting=items[4]
                )

                assignment.save()
            except:
                print('AN ERROR HAS OCCURED')

    def __str__(self):
        return self.title
