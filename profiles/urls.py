from django.urls import path
from . import views

urlpatterns = [
    path('<int:user_id>', views.dashboard, name='dashboard'),
    path('create_assignment',
         views.create_assignment, name='create_assignment')
]
