MODULE_COLOURS = [
    '#d15555',
    '#9978ed',
    '#55d188',
    '#c9cc3d',
    '#cc72c9',
    '#43b5c6'
]

MODULE_STYLES = ['style=background:{}'.format(
    colour) for colour in MODULE_COLOURS]

MODULE_ICONS = [
    'fa-plane',
    'fa-book',
    'fa-calculator',
    'fa-briefcase',
    'fa-calendar'
]
